const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { InternalException } = require('../services/internalException');

const router = Router();

// TODO: Implement route controllers for user
router.get('/', (req, res, next) => {
    try {
        const data=UserService.getAll();
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
},responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const data=UserService.getById(req.params.id);
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
},responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
    try {
        if(res.err)
        {
            throw new InternalException(res.err.code,res.err.message);
        }
        const user=UserService.create(req.body);
        res.data = user;
    } catch (err) {        
        res.err = err;
    } finally {
        next();
    }
},responseMiddleware);

router.put('/:id', updateUserValid,(req, res, next) => {
    try {
        if(res.err)
        {
            throw new InternalException(res.err.code,res.err.message);
        }
        const data=UserService.update(req.params.id,req.body);
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
},responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        const data=UserService.delete(req.params.id);
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
},responseMiddleware);

module.exports = router;