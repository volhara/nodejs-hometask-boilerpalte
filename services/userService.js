const { UserRepository } = require('../repositories/userRepository');
const { search } = require('./fighterService');
const { InternalException } = require('./internalException');

class UserService {

    getAll()
    {
        const users=UserRepository.getAll();
        return users;
    }

    getById(id)
    {
        const user=this.search({id});
        if(!user){
            throw new InternalException(404,'User not found');
        }
        return user;
    }

    create(user)
    {
        const uniqueFields=['email','phoneNumber'];

        for (const field of uniqueFields) {
            const existUser=this.search({[field]:user[field]});
            if(existUser)
            {
                throw new InternalException(400,`User with this ${field} is already exist`);
            }
        }

        const createdUser = UserRepository.create(user);
        if(!createdUser)
        {
            throw new InternalException(400,'Error creating user');
        }
        return createdUser;
    }

    update(id, user)
    {
        const uniqueFields=['email','phoneNumber'];
        
        for (const field of uniqueFields) {
            const existUser=this.search({[field]:user[field]});
            if(existUser)
            {
                throw new InternalException(400,`User with this ${field} is already exist`);
            }
        }

        const updatedUser= UserRepository.update(id,user);
        if(!updatedUser)
        {
            throw new InternalException(400,'Error updating user');
        }
        return updatedUser;
    }

    delete(id)
    {
        const  deletedUser=UserRepository.delete(id);
        if(!deletedUser)
        {
            throw new InternalException(404,'Error deleting user');
        }
        return deletedUser
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();