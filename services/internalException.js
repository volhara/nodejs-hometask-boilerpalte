class InternalException
{
  constructor(code, message) {
    this.code = code;
  this.message = message;
}
}
exports.InternalException = InternalException;