const { FighterRepository } = require('../repositories/fighterRepository');
const { InternalException } = require('./internalException');

class FighterService {
    // TODO: Implement methods to work with fighters
    getAll()
    {
        const items=FighterRepository.getAll();
        return items;
    }

    getById(id)
    {
        const fighter=this.search({id});
        if(!fighter){
            throw new InternalException(404,'Fighter not found');
        }
        return fighter;
    }

    create(fighter)
    {
        const uniqueFields=['name'];
        
        for (const field of uniqueFields) {
            const existFighter=this.search({[field]:fighter[field]});
            if(existFighter)
            {
                throw new InternalException(400,'Fighter already exist');
            }
        }

        const createdFighter = FighterRepository.create(fighter);
        if(!createdFighter)
        {
            throw new InternalException(400,'Error creating fighter');
        }
        return createdFighter;
    }

    update(id, fighter)
    {
        const uniqueFields=['name'];
        
        for (const field of uniqueFields) {
            const existFighter=this.search({[field]:fighter[field]});
            if(existFighter)
            {
                throw new InternalException(400,'Fighter already exist');
            }
        }

        const updatedFighter= FighterRepository.update(id,fighter);
        if(!updatedFighter)
        {
            throw new InternalException(400,'Error updating fighter');
        }
        return updatedFighter;
    }

    delete(id)
    {
        const deletedFighter=FighterRepository.delete(id);
        if(!deletedFighter)
        {
            throw new InternalException(404,'Error deleting fighter');
        }
        return deletedFighter;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();