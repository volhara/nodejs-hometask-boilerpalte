const { isNumber } = require('lodash');
const { fighter } = require('../models/fighter');
const { InternalException } = require('../services/internalException');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    try {
        if (!req.body) {
            throw new InternalException(400, 'Body is empty');
        }
        const notRequire = ['health', 'id'];

        for (const propName of Object.keys(fighter)) {
            if (!notRequire.includes(propName) && (!Object.keys(req.body).includes(propName) || !req.body[propName])) {
                throw new InternalException(400, `${propName} is require field`);
            }
        }

        for (const propName of Object.keys(req.body)) {
            if (!Object.keys(fighter).includes(propName) || propName === 'id') {
                throw new InternalException(400, `${propName} is unexpected field`);
            }
        }

        if (!req.body.health) {
            req.body.health = 100;
        }

        if (!isNumber(req.body.power)) {
            throw new InternalException(400, 'Power must be a number');
        }

        if (!(req.body.power < 100 && req.body.power > 1)) {
            throw new InternalException(400, 'Power must be 1<power<100');
        }

        if (!isNumber(req.body.defense)) {
            throw new InternalException(400, 'Defense must be a number');
        }

        if (!(req.body.defense < 10 && req.body.defense > 1)) {
            throw new InternalException(400, 'Defense must be 1<defense<10');
        }

        if (!isNumber(req.body.health)) {
            throw new InternalException(400, 'Health must be a number');
        }

        if (!(req.body.health < 120 && req.body.health > 80)) {
            throw new InternalException(400, 'Health must be 80<health<120');
        }

    } catch (error) {
        res.err = error;
    } finally {
        next();
    }
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    try {
        if (!req.body) {
            throw new InternalException(400, 'Body is empty');
        }
        for (const propName of Object.keys(req.body)) {
            if (!Object.keys(fighter).includes(propName) || propName === 'id') {
                throw new InternalException(400, `${propName} is unexpected field`);
            }
        }
        let isAny = false;
        for (const propName of Object.keys(fighter)) {
            if (req.body[propName]) {
                isAny = true;
                break;
            }
        }
        if (!isAny) {
            throw new InternalException(400, 'Body must have field for update');
        }

        if (req.body.power && !isNumber(req.body.power)) {
            throw new InternalException(400, 'Power must be a number');
        }

        if (req.body.power && !(req.body.power < 100 && req.body.power > 1)) {
            throw new InternalException(400, 'Power must be 1<power<100');
        }

        if (req.body.defense && !isNumber(req.body.defense)) {
            throw new InternalException(400, 'Defense must be a number');
        }
        if (req.body.defense && !(req.body.defense < 10 && req.body.defense > 1)) {
            throw new InternalException(400, 'Defense must be 1<defense<10');
        }

        if (req.body.health && !isNumber(req.body.health)) {
            throw new InternalException(400, 'Health must be a number');
        }

        if (req.body.health && !(req.body.health < 120 && req.body.health > 80)) {
            throw new InternalException(400, 'Health must be 80<health<120');
        }
    } catch (error) {
        res.err = error;
    } finally {
        next();
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;