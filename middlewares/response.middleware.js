const { error } = require('../models/error');
const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
   if(res.err)
   {
       error.error=true;
       error.message=res.err.message;
       res.status(res.err.code).send(error);
   }else
   {
       res.send(res.data);
   }
   next();
}

exports.responseMiddleware = responseMiddleware;