const { user } = require('../models/user');
const { InternalException } = require('../services/internalException');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    try {
        if (!req.body) {
            throw new InternalException(400, 'Body is empty');
        }

        for (const propName of Object.keys(user)) {
            if (propName!=='id'&&(!Object.keys(req.body).includes(propName) || !req.body[propName])) {
                throw new InternalException(400, `${propName} is require field`);
            }
        }

        for (const propName of Object.keys(req.body)) {
            if (!Object.keys(user).includes(propName) || propName === 'id') {
                throw new InternalException(400, `${propName} is unexpected field`);
            }
        }

        if (!(req.body.password.length > 3)) {
            throw new InternalException(400, 'Password must have min 3 char');
        }

        if (!validateEmail(req.body.email)) {
            throw new InternalException(400, 'Email is not valid');
        }

        if (!validatePhoneNumber(req.body.phoneNumber)) {
            throw new InternalException(400, 'Phone number is not valid');
        }
    } catch (error) {
        res.err = error;
    } finally {
        next();
    }
}

function validatePhoneNumber(phoneNumber) {
    let re = /^\+?3?8?(0[5-9][0-9]\d{7})$/gi;
    return re.test(phoneNumber);
}

function validateEmail(email) {
    let re = /@gmail\.com$/;
    return re.test(email);
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    try {
        if (!req.body) {
            throw new InternalException(400, 'Body is empty');
        }
        for (const propName of Object.keys(req.body)) {
            if (!Object.keys(user).includes(propName) || propName === 'id') {
                throw new InternalException(400, `${propName} is unexpected field`);
            }
        }
        let isAny = false;
        for (const propName of Object.keys(user)) {
            if (req.body[propName]) {
                isAny = true;
                break;
            }
        }
        if (!isAny) {
            throw new InternalException(400, 'Body must have field for update');
        }

        if (req.body.phoneNumber && !validatePhoneNumber(req.body.phoneNumber)) {
            throw new InternalException(400, 'Phone number is not valid');
        }

        if (req.body.email && !validateEmail(req.body.email)) {
            throw new InternalException(400, 'Email is not valid');
        }

        if (req.body.password && !(req.body.password.length > 3)) {
            throw new InternalException(400, 'Password must have min 3 char');
        }
    } catch (error) {
        res.err = error;
    } finally {
        next();
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;